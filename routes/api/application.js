var express = require('express');

var config = require('../../config');
var User = require('../../models/user');

var router = express.Router();

router.post('/', function(req, res, next) {
    // Create new user or update it if there's already existing user with same email
    User.findOne({email: req.body.email}).exec().then(function(user) {
        if (user == null) {
            User.create(req.body).then(function() {
                res.status(200).send(config.successMessage);
            }, next);
        } else {
            user.name = req.body.name;
            user.os = req.body.os;
            user.save().then(function() {
                res.status(200).send(config.successMessage);
            }, next);
        }
    }, next);
});

module.exports = router;