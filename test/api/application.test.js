var request = require('supertest');
var expect = require('chai').expect;
var mongoose = require('mongoose');

var config = require('../../config');
var app = require('../../app');
var User = require('../../models/user');

describe('#/api/application', function() {

    var log = console.log;

    before(function() {
        console.log = function() {};

        mongoose.connection.once('open', function(callback) {
            mongoose.connection.db.dropDatabase();

            if (callback)
                callback();
        });
    });

    after(function() {
        mongoose.connection.db.dropDatabase();
    });

    it('new entity saved and returns config.successMessage', function(done) {
        request(app)
            .post('/api/application')
            .send({name: 'Test User', email: 'test@user.com', os: 'windows'})
            .end(function(err, res) {
                console.log = log;
                expect(err).to.be.null;
                expect(res.status).to.be.equal(200);
                expect(res.text).to.be.equal(config.successMessage);

                // check that entity was stored in db
                User.findOne({email: 'test@user.com'}).exec().then(function(user) {
                    expect(user.name).to.be.equal('Test User');
                    expect(user.os).to.be.equal('windows');
                    done();
                });
            });
    });

    it('existing entity updated and returns config.successMessage', function(done) {
        request(app)
            .post('/api/application')
            .send({name: 'Test User 2', email: 'test@user.com', os: 'macos'})
            .end(function(err, res) {
                console.log = log;
                expect(err).to.be.null;
                expect(res.status).to.be.equal(200);
                expect(res.text).to.be.equal(config.successMessage);

                // check that entity was updated in db
                User.findOne({email: 'test@user.com'}).exec().then(function(user) {
                    expect(user.name).to.be.equal('Test User 2');
                    expect(user.os).to.be.equal('macos');
                    done();
                });
            });
    });

    it('entity fails validation and returns error with message', function(done) {
        request(app)
            .post('/api/application')
            .send({email: 'foo'})
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res.status).to.be.equal(500);
                expect(res.body.message).to.exist;

                // check that entity was not stored in db
                User.findOne({email: 'foo'}).exec().then(function(user) {
                    expect(user).to.be.null;
                    done();
                });
            });
    });
});