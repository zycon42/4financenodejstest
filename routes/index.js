var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', bullets: [
        "Ipsum dolor sit amet",
        "Lorem ipsum dolor sit amet lorem",
        "Dolor sit dolor site amet",
        "Lorem dolor sit amet",
        "Lipsum Lorem dolor sit"
    ] });
});

module.exports = router;
