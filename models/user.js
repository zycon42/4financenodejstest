var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var emailMatch = [
    /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    'This is not valid email address'
];

var userSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true, match: emailMatch},
    os: {type: String, required: true, enum: ['windows', 'macos']}
});

module.exports = mongoose.model('User', userSchema);
